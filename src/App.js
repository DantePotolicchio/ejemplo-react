import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from './Components/Header/Header';
import Navbar from './Components/Navbar/Navbar';
import Home from './Components/Home/Home';
import Curiosidades from './Components/Curiosidades/Curiosidades';
import Random from './Components/Random/Random';
import Footer from './Components/Footer/Footer';
import Modal from './Components/Modal/Modal';
import './App.css';

function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/curiosidades">
          <Navbar active="Curiosidades" />
          <Curiosidades />
        </Route>
        <Route path="/random">
          <Navbar active="Random" />
          <Random />
        </Route>
        <Route path="/">
          <Navbar active="Home" />
          <Home />
        </Route>
      </Switch>
      <Footer />
      <Modal />
    </Router>
  );
}

export default App;
