import React from 'react';
import camaleonOMG from '../../img/camaleonOMG.jpg';
import serpiente from '../../img/serpiente.jpg';
import gatoVScocodrilo from '../../img/gatoVScocodrilo.jpg';

export default function Curiosidades() {
    return (
        <div className="my-4 container-fluid">
            <div className="row">
                <div className="col pl-5">
                    <h2 className="my-3 text-white font-weight-bold">10 curiosidades sobre los reptiles que debes conocer</h2>
                    <ol className="bg-light py-3">
                        <li>
                            <h5>Viven en casi todo el mundo. Los reptiles son uno de los grupos más diversos entre los vertebrados terrestres de nuestro planeta.</h5>
                            <p>Los reptiles son uno de los grupos más diversos entre los vertebrados terrestres de nuestro planeta. Hoy en día se conocen más de 8000 especies diferentes, entre lagartos, serpientes, tortugas y cocodrilos, que habitan en todos los continentes de la Tierra con excepción de la Antártida, donde el clima es extremadamente frío para ellos.</p>
                        </li>
                        <li>
                            <h5>Serpientes venenosas</h5>
                            <p>Pese a su mala fama, la mayoría de las serpientes, aproximadamente las dos terceras partes de las existentes, no son venenosas y de ellas apenas entre 30 y 40 especies pueden representar un peligro para los seres humanos (menos del 2% del total). Eso sí, en Australia la proporción se invierte, allí la mayoría de las serpientes son venenosas, así que si tienes la oportunidad de ir ¡ten mucho cuidado!</p>
                        </li>
                        <li>
                            <h5>Tortugas bajo el hielo</h5>
                            <p>Si bien es cierto que los reptiles son poco tolerantes a las bajas temperaturas y la mayoría de ellos disfrutan más del cálido sol de las zonas tropicales, existe una tortuga que habita en los Grandes Lagos de Estados Unidos que es capaz de vivir y nadar bajo el hielo, algo impensable en otros de sus semejantes. Su nombre es Tortuga de Blanding (Emydoidea blandingii).</p>
                        </li>
                        <li>
                            <h5>Serpientes de gran talla</h5>
                            <p>La pitón reticulada es la serpiente más larga del mundo, ya que puede superar los 10 m de longitud, pero la anaconda, que puede pesar más de 140 kg, puede considerarse la serpiente más grande en cuanto a proporción talla-peso. Sin embargo, entre las venenosas, el récord es de la cobra real, que puede medir hasta 6 m.</p>
                        </li>
                        <li>
                            <h5>Animales longevos por excelencia</h5>
                            <p>Algunas de las especies más longevas del planeta son reptiles. Es legendaria ya la longevidad que pueden alcanzar algunas tortugas, capaces de vivir más de 150 años, pero otras especies también tienen importantes registros, por ejemplo, algunos cocodrilos han llegado a superar los 70 años y las pitones alrededor de 40.</p>
                        </li>
                        <li>
                            <h5>Los reptiles no tienen la sangre fría</h5>
                            <p>A pesar de que es común decir que los reptiles “tienen la sangre fría”, realmente no es así. Simplemente, al ser animales ectotérmicos, estos obtienen el calor corporal a partir de fuentes externas, como puede ser el sol. Esto las diferencia de los llamados animales de “sangre caliente”, como las aves y los mamíferos, que regulamos la temperatura interna por nosotros mismos, independientemente del entorno.</p>
                        </li>
                        <li>
                            <h5>No son una amenaza</h5>
                            <p>Aunque hemos avanzado en la educación ambiental, los reptiles aún son considerados por miles de personas como una amenaza mayor que muchas otras especies que en verdad han demostrado ser más peligrosas para los humanos a lo largo de la historia. Por ejemplo, cada año mueren más estadounidenses a causa de las picaduras de abejas que de mordeduras de serpientes y, sin embargo, no son muchos los que colocan a las abejas entre sus animales más temidos.</p>
                        </li>
                        <li>
                            <h5>Son animales muy antiguos</h5>
                            <p>Los reptiles están sin ninguna discusión entre las joyas de la evolución del reino animal y están prácticamente desde los inicios de la conquista del medio terrestre. Las tortugas, por ejemplo, han estado en la Tierra más de 200 millones de años y han permanecido básicamente de la misma forma en que las vemos hoy en día.</p>
                        </li>
                        <li>
                            <h5>Los reptiles no son viscosos</h5>
                            <p>Para muchas personas, los reptiles son desagradables porque son animales viscosos. Esto está totalmente alejado de la realidad ya que, al no tener glándulas sudoríparas, su piel se mantiene todo el tiempo seca y fresca.</p>
                        </li>
                        <li>
                            <h5>La extraña cabeza de las serpientes</h5>
                            <p>El cráneo de las serpientes está compuesto por muchos huesos pequeños que están interconectados de un modo muy flexible, a diferencia, por ejemplo, del de los humanos, que está formado por una pieza sólida de huesos fuertemente fusionados. Esto les permite acomodarlos voluntariamente y abrir su boca hasta alcanzar proporciones asombrosas a la hora de engullir grandes presas. En esto destacan las especies constrictoras.</p>
                        </li>
                    </ol>
                </div>
                <div className="col">
                    <img src={gatoVScocodrilo} alt=". . ." className="w-100"></img>
                    <img src={camaleonOMG} alt="Camaleón sorprendido" className="w-100"></img>
                    <img src={serpiente} alt="Serpiente" className="w-100"></img>
                </div>
            </div>
        </div>
    );
}