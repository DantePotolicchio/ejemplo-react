import React from 'react';
import './Footer.css';

export default function Footer() {
    return(
        <footer id="footer" className="bg-black text-white text-center mt-4 p-4">
            <p>Dante Potolicchio 2020 &copy; todos los derechos reservados</p>
        </footer>
    );
}