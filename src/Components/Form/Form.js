import React from 'react';

export default function Form() {
    return(
        <form onSubmit={(e) => e.preventDefault()}>
            <div className="form-group">
                <label htmlFor="email">Ingrese su correo para recibir nuestras novedades</label>
                <input type="email" className="form-control" placeholder="alguien@ejemplo.com" />
            </div>
        </form>
    );
}