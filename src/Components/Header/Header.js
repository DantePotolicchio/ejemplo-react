import React from 'react';
import './Header.css';
import Logo from '../Logo/Logo';
import logo from './../../img/logo.png';
import { Link } from 'react-router-dom';


export default function Header() {
  return (
    <nav className="navbar navbar-dark d-flex bg-black">
      <Link to="/">
        <Logo
          src={logo}
          clase="my-2 ml-3 d-inline"
          alt="Logo Rolling Code"
          width={150}
          heigth={150}
        />
      </Link>
      <h1 className="d-inline text-white mx-auto font-italic text-monospace titulo">Reptiles</h1>
    </nav>
  );
}