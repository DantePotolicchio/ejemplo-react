import React from 'react';
import Carrousel from '../Carrousel/Carrousel';

export default function Main() {
    return (
        <main className="container-fluid">
            <div className="row">
                <div className="col-12 col-lg-4 d-flex flex-column">
                    <p className="bg-light text-left ml-3 p-2">Los reptiles son una clase de animales vertebrados amniotas provistos de escamas epidérmicas de queratina. 
                    Fueron muy abundantes en el Mesozoico, época en la que surgieron los dinosaurios, pterosaurios, ictiosaurios, plesiosaurios y mosasaurios.</p>
                    <h3 className="text-white font-weight-bold ml-5 mb-3">Características</h3>
                    <p className="bg-light text-left ml-3 p-2">La mayoría de los reptiles se han adaptado a la vida terrestre, pero finalmente se ha descubierto que algunos viven en el agua. 
                    Una piel resistente y escamosa es una de sus adaptaciones. Otras de las adaptaciones que han contribuido al éxito de los reptiles en tierra firme son que incluyen 
                    pulmones bien desarrollados, un sistema circulatorio de doble circuito, un sistema excretor que conserva el agua, fuertes extremidades, fertilización interna y huevos 
                    terrestres con cascarón. Además los reptiles pueden controlar su temperatura corporal cambiando de lugar.</p>
                </div>
                <div className="col-12 col-lg-8">
                    <Carrousel />
                </div>
            </div>
        </main>
    );
}