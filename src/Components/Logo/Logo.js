import React from 'react';

export default function Logo(props) {
    return (
        <img 
            src={props.src} 
            className={props.clase || ""} 
            alt={props.alt} 
            width={props.width || ""} 
            height={props.height || ""}
        >
        </img>
    );
}