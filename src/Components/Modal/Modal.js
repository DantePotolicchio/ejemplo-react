import React from 'react';
import Form from '../Form/Form';

export default function Modal() {
    return (
        <div className="modal fade" id="modalSubscripcion" tabIndex="-1" role="dialog" aria-labelledby="modalSubscripcionLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="modalSubscripcionLabel">Subscripción</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <Form />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" className="btn btn-success">Subscribirme!</button>
                    </div>
                </div>
            </div>
        </div>
    );
}