import React from 'react';
import { Link } from 'react-router-dom';

export default function Navbar( { active } ) {
    return(
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item ml-3"><button className="btn btn-success" type="button" data-toggle="modal" data-target="#modalSubscripcion">Recibir Noticias</button></li>
            <li className="breadcrumb-item ml-auto"><Link to="/" className={active === "Home" ? "text-reset text-decoration-none font-weight-bold" : ""}>Home</Link></li>
            <li className="breadcrumb-item"><Link to="/curiosidades" className={active === "Curiosidades" ? "text-reset text-decoration-none font-weight-bold" : ""}>Curiosidades</Link></li>
            <li className="breadcrumb-item"><Link to="/random" className={active === "Random" ? "text-reset text-decoration-none font-weight-bold" : ""}>Random</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{Date().toString().replace("Mon", "Lun").slice(0, 21)}</li>
          </ol>
        </nav>
    );
}