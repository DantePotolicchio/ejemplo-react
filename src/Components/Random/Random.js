import React from 'react';
import lagartijaBailando from '../../img/lagartijaBailando.mp4';
import './Random.css';

export default function Random() {
    return (
        <div className="d-flex justify-content-center">
            <video id="video" src={lagartijaBailando} autoPlay height="650" loop controls>
            </video>
        </div>
    );
}
